var app = new Vue({
    el: "#mainApp",
    data: {
        schedules: "",
        contacts: "",
        days: [],
        months: [],
        portrait: 1,
        landscape: 0,
        selectedMatch: {
            Team1: "",
            Team2: "",
            Time: "",
            Location: "",
            Address: "",
            src: "",
        },
        height: "",
        logged: 0,
        unlogged: 1,
        selectedMonth: "September",
        user: "",
        currentUID: "",
        messageText: "",
        allPost:[]
    },
    created() {
        this.schedule();
        this.handleResize();
        window.addEventListener('resize', this.handleResize);
    },
    destroyed() {
        window.removeEventListener('resize', this.handleResize());
    },
    methods: {
        schedule: function() {
            //let that = this;
            fetch('./json/data.json')
                .then(function(response) {
                    return response.json();
                })
                .then(function(myJson) {
                    this.app.schedules = myJson.Schedule;
                    this.app.contacts = myJson.Contacts;
                    return myJson;
                });
        },
        changeSelection: function(month) {
            this.selectedMonth = month;
        },
        compareDate: function(dateOne, dateTwo) {
            if(dateOne == dateTwo) return true;
            return false;
        },
        handleResize: function(e) {
            console.log("Handling resize");
            height = window.innerHeight - (56 + 38 + 28);
            if(window.innerHeight > window.innerWidth) {
                this.portrait = 1;
                this.landscape = 0;
            } else{
                this.portrait = 0;
                this.landscape = 1;
            }
        },
        selectMatch: function(match){
            this.selectedMatch.Team1 = match.Team1;
            this.selectedMatch.Team2 = match.Team2;
            this.selectedMatch.Time = match.Time;
            this.selectedMatch.Location = match.Location;
            this.selectedMatch.Address = match.Address;
            this.selectedMatch.src = match.src;
        },
        googleAuth: function() {
            let modal = document.getElementById('Login');
            let btn = document.querySelectorAll('.sign-in');
            let modalBackdrop = document.querySelector('.modal-backdrop');

            btn[0].addEventListener('click', function() {
                modal.classList.remove('show');
                modalBackdrop.classList.remove('show');
            });
            btn[1].addEventListener('click', function() {
                modal.classList.remove('show');
                modalBackdrop.classList.remove('show');
            });

            let that = this;
            var provider = new firebase.auth.GoogleAuthProvider();
            firebase.auth().signInWithPopup(provider).then(function(result) {
                this.user = result.user;
                this.currentUID = result.credential.uid;
                that.logged = 1;
                that.unlogged = 0;
            });
        },
        anonymousAuth:function(){
            let that = this;
            firebase.auth().signInAnonymously().then(function(result) {
                this.user = "Anonymous";
                that.logged = 1;
                that.unlogged = 0;
            });
        },
        signoutAuth:function(){
            let that = this;
            firebase.auth().signOut();
            that.logged = 0;
            that.unlogged = 1;
            user = "";
        },
        newPost:function(){
          firebase.database().ref('post').push({
            titulo: "test",
            mensaje: this.messageText,
            nombre: this.user || "Anonymous",
            fechaYhora: moment().format('DD/MM/YYYY HH:mm'),
            uid: this.currentUID
          });
        },
        getPost:function(){
            if(this.logged == 1){
                this.allPost = [];
                var post = firebase.database().ref('post');
                post.on("child_added", (datos) => {
                    console.log(datos.val());
                    this.allPost.push(datos.val());
                }, (errorObject) => {
                    console.log("La Lectura Falla: " + errorObject.code);
                });
            }
        },
        closeConnection:function(){
            var post = firebase.database().ref('post');
            post.off();
        }
    }
});